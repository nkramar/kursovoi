﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace kursovoyStudent
{
    /// <summary>
    /// Класс Студент
    /// </summary>
    public class Student: IComparable
    {
        string fIO;
        string nameGroup;
        int stipendia;
        Kategoria_enum kategoria;

        /// <summary>
        /// ФИО студента
        /// </summary>
        public string FIO
        {
            get
            {
                return fIO;
            }
            set
            {
                string[] strmas = value.Split(new char[] { ' ', '.' });

                string fam = "";
                string name = "";
                string otch = "";

                int i = 1;
                // получаем по отдельности фамилию имя и отчество
                foreach (string s in strmas)
                {
                    if (s.Length != 0)
                    {
                        switch (i)
                        {
                            case 1:
                                fam = s;
                                i++;
                                break;
                            case 2:
                                name = s;
                                i++;
                                break;
                            case 3:
                                otch = s;
                                i++;
                                break;
                            default:
                                throw new Exception("Ошибка. ФИО должно иметь формат: Фамилия И.О.");
                        }
                    }
                }

                if (i != 4)
                {
                    throw new Exception("Ошибка. ФИО должно иметь формат: Фамилия И.О.");
                }

                if (fam.Length < 2)
                {
                    throw new Exception("Ошибка. Фамилия не может содержать меньше чем 2 си");
                }

                fIO = value;
            }
        }

        /// <summary>
        /// Название группы
        /// </summary>
        public string NameGroup
        {
            get
            {
                return nameGroup;
            }
            set
            {
                int i = int.Parse("df");
                System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex("\\b[а-яА-я]{2,3}-\\d\\d\\b");
                if (reg.IsMatch(value))
                    nameGroup = value;
                else throw new Exception("Неверное имя группы");
            }
        }

        /// <summary>
        /// Размер стипендии
        /// </summary>
        public int Stipendia
        {
            get
            {
                return stipendia;
            }
            set
            {

                if (value < 0)
                {
                    throw new Exception(string.Format("Ошибка: Стипендия не может быть отрицательной у {0}",this.FIO));
                }

                stipendia = value;
            }
        }

        /// <summary>
        /// Категория студента
        /// </summary>
        public Kategoria_enum Kategoria
        {
            get
            {
                return kategoria;
            }
            set
            {
                kategoria = value;
            }
        }


        #region Конструкторы

        /// <summary>
        /// Полный конструктор
        /// </summary>
        /// <param name="fio">ФИО студента</param>
        /// <param name="namegroup">название группы</param>
        /// <param name="stipendia">размер стипендии</param>
        /// <param name="kategoria">категория студента</param>
        public Student(string fio, string namegroup, int stipendia, Kategoria_enum kategoria)
        {
            this.FIO = fio;
            this.NameGroup = namegroup;
            this.Stipendia = stipendia;
            this.Kategoria = kategoria;
        }

        public Student(string stud)
        {
            string[] str = stud.Split(new char[] {';'});

            this.FIO = str[0];
            this.NameGroup = str[1];

            int stip;
            if (int.TryParse(str[2], out stip) == false)
            {
                throw new Exception("Ошибка. В поле стипендия не целое число");
            }
            this.Stipendia = stip;

            Kategoria_enum kat;

            if (Enum.TryParse<Kategoria_enum>(str[3], out kat) == false)
            {
                throw new Exception("Ошибка. Не верно указана категория студента");
            }
            this.Kategoria = kat;


        }

        /// <summary>
        /// Конструктор, где известно только ФИО и категория
        /// </summary>
        /// <param name="fio">ФИО студента</param>
        /// <param name="kategoria">категория студента</param>
        public Student(string fio, Kategoria_enum kategoria) 
            : this(fio,"Не определена",0,kategoria)
        { }

        /// <summary>
        /// Конструктор без параметров
        /// </summary>
        public Student()
            : this("Не определён", Kategoria_enum.Городской)
        { }


        #endregion

        #region Перегрузка операторов

        #region Бинарные операторы
        /// <summary>
        /// Бинарный оператор сложения
        /// Объединяет ФИО двух студентов через дефис
        /// берёт группу по первому студенту
        /// Стипендии суммирует
        /// Категорию студента берёт у первого студента
        /// </summary>
        /// <param name="st1">Первый студент</param>
        /// <param name="st2">Второй студент</param>
        /// <returns></returns>
        public static Student operator +(Student st1, Student st2)
        {
            Student temp = new Student();

            temp.FIO =st1.FIO + " - " + st2.FIO;
            temp.NameGroup = st1.NameGroup;
            temp.Stipendia = st1.Stipendia + st2.Stipendia;
            temp.Kategoria = st1.Kategoria;

            return temp;
        }
        #endregion

        #region Оператор отношения
        /// <summary>
        /// Оператор отношения, сравнивает размер стипендий студентов
        /// </summary>
        /// <param name="st1">Первый студент</param>
        /// <param name="st2">Второй студент</param>
        /// <returns>результат сравнения</returns>
        public static bool operator <(Student st1, Student st2)
        {
            return st1.Stipendia < st2.Stipendia;
        }
        public static bool operator >(Student st1, Student st2)
        {
            return st1.Stipendia > st2.Stipendia;
        }
        public static bool operator <=(Student st1, Student st2)
        {
            return st1.Stipendia <= st2.Stipendia;
        }
        public static bool operator >=(Student st1, Student st2)
        {
            return st1.Stipendia >= st2.Stipendia;
        }

        #endregion

        #region Оператор инкремента
        /// <summary>
        /// С помощью оператора инкремента мы будем переводить студента на следующий курс
        /// </summary>
        /// <param name="st">Студент</param>
        /// <returns>Студент на следующем курсе</returns>
        public static Student operator ++(Student st)
        {
            string str = st.NameGroup;      //получаем копию названия группы

            char[] mas = str.ToCharArray();  // разбиваем название группы на одельные символы

            for (int i = 0; i < mas.Length; i++)    // проходим по всем символам массива
            {
                if (char.IsDigit(mas[i]))       //ищем цифру
                {
                    byte b = Convert.ToByte(mas[i]);        //преобразуем эту цифру в байт
                    b++;                                // увеличиваем цифру на 1
                    char c;
                    c = Convert.ToChar(b);              // получаем снова символ нашей обновлённой цифры
                    str = str.Remove(i,1);              // удаляем из названия группы первую цифру
                    str = str.Insert(i,c.ToString());   // вставляем в название группы новую цифру
                    break;                              // можно выходить из цикла
                }
            }

            st.NameGroup = str;         // заносим в поле номера группы новое значение, которое мы сформировали в цикле

            return st;          //возвращаем полученного студента.
        }
        #endregion

        #region Операторы >> и <<

        /// <summary>
        /// Оператор сдвига увеличивает или уменьшает стипендию,
        /// Количество перемещений равно значению переменной i
        /// </summary>
        /// <param name="kaf1"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        public static Student operator <<(Student  st, int i)
        {
            Student  temp = new Student();

            temp.FIO = st.FIO;
            temp.NameGroup = st.NameGroup;
            temp.Kategoria = st.Kategoria;

            int stip;

            stip = st.Stipendia - i;

            if (stip < 0)
            {
                temp.stipendia = 0;
            }
            else
            {
                temp.stipendia = stip;
            }


           return temp;
        }

        public static Student operator >>(Student st, int i)
        {
            Student temp = new Student();

            temp.FIO = st.FIO;
            temp.NameGroup = st.NameGroup;
            temp.Kategoria = st.Kategoria;

            int stip;

            stip = st.Stipendia + i;

            if (stip < 0)
            {
                temp.stipendia = 0;
            }
            else
            {
                temp.stipendia = stip;
            }


            return temp;
        }


        #endregion

        #endregion




        public int CompareTo(object obj)
        {
            Student st = (Student)obj;

            if (this > st)
                return 1;
            else
                if (this < st)
                    return -1;
                else
                    return 0;
        }
    }
}
