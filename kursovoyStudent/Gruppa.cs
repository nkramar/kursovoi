﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace kursovoyStudent
{

    public class Gruppa
    {
     

        List<Student> Massiv = new List<Student>();

        /// <summary>
        /// Студент с максимальной стипендией
        /// </summary>
        public Student Max
        {
            get
            {
                if (Massiv.Count == 0)
                {
                    return null;
                }
                Student MaxStud = Massiv[0];
                for (int i = 1; i < Massiv.Count; i++)
                {
                    if (Massiv[i] > MaxStud)
                    {
                        MaxStud = Massiv[i];
                    }
                }
                return MaxStud;
            }
        }

        public int Count
        {
            get { return Massiv.Count; }
        }

        public void Add(Student s)
        {
            Massiv.Add(s);
        }

        public void Remove(Student s)
        {
            Massiv.Remove(s);
        }

    }
}
